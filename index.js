const puppeteer = require("puppeteer")
const Keyv = require('keyv');
const KeyvFile = require('keyv-file').KeyvFile

const db = new Keyv({
    store: new KeyvFile({
        filename: "db.json"
    })
});

const iPhoneAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit / 602.1.50(KHTML, like Gecko) CriOS / 56.0.2924.75 Mobile / 14E5239e Safari / 602.1"
const Pixel2Agent = "Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.125 Mobile Safari/537.36"

const date = new Date(new Date().toLocaleString("en-US", { timeZone: "Asia/Tehran" }));

async function screenshot(ymUrl, myUrl) {

    const baseFileName = `screenshots/${date.getMonth()}-${date.getDay()}-${date.getHours()}-${date.getMinutes()}`;
    const ymFile = baseFileName + "-ym.png"
    const myFile = baseFileName + "-my.png"

    // Launching the browser
    const browser = await puppeteer.launch({
        headless: true,
        defaultViewport: {
            width: 411,
            height: 823,
            isLandscape: false
        },
        args: ['--lang=en-GB']
    });

    const page = await browser.newPage();
    page.setUserAgent(iPhoneAgent)
    page.emulateTimezone("Asia/Tehran")
    page.setExtraHTTPHeaders(
        {
            'Accept-Language': 'en-GB'
        }
    )
    await page.setViewport({
        width: 411,
        height: 823,
        isLandscape: false,
        isMobile: true
    })

    page.emulateMediaType("screen")
    // Yadegar - Modarres
    await page.goto(
        ymUrl,
        { waitUntil: "networkidle2" }
    );

    await page.content();

    // Close Google Ad
    let appbutton = await page.waitForSelector('button.ml-promotion-no-thanks')
    await page.click(appbutton._remoteObject.description)
    await page.waitForTimeout(1000);


    await page.screenshot({ path: ymFile, fullPage: true })





    let ym_duration = await (await page.$("span.ml-directions-pane-header-time-content")).getProperty("innerText")


    await page.goto(
        myUrl,
        { waitUntil: "networkidle2" }
    )
    await page.content();
    await page.screenshot({ path: myFile, fullPage: true })

    let my_duration = await (await page.$("span.ml-directions-pane-header-time-content")).getProperty("innerText")
    await browser.close()

    Promise.all([
        ym_duration.jsonValue(),
        my_duration.jsonValue(),
    ]).then((values) => {
        saveResults(values[0], values[1])
    })
}

function saveResults(ym, my) {

    const baseKey = `${date.getMonth()}-${date.getDay()}|${date.getHours()}:${date.getMinutes()}:`
    db.set(baseKey + "ym",  ym.trim())
    db.set(baseKey + "my", my.trim())

}

const ymUrl = "https://www.google.com/maps/dir/Hemmat+Expy,+Tehran+Province,+Tehran,+Iran/35.7499916,51.4203073/@35.7506757,51.3498803/am=t/data=!3m1!4b1!4m8!4m7!1m5!1m1!1s0x3f8e07808cbca2e1:0x4b4ae4047cf7d3a3!2m2!1d51.3482631!2d35.75094!1m0?shorturl=1"
const myUrl = "https://www.google.com/maps/dir/Hemmat+Expy,+Tehran+Province,+Tehran,+Iran/35.7511485,51.3487516/@35.7510198,51.3645718/data=!4m9!4m8!1m5!1m1!1s0x3f8e06bb6a0dc269:0x66827fa48fc93151!2m2!1d51.4184202!2d35.7513355!1m0!3e0"

//screenshot("https://www.google.com/maps/dir/35.750916,51.348696/35.750059,51.420205/@35.7522864,51.3901977/data=!4m2!4m1!3e0", )
screenshot(ymUrl, myUrl)